const { serverID, adminUser } = require("../config/config");

const Discord = require("discord.js");

// Roles
const roles = require("../config/roles_annee");

// Etapes inscription
const etapes = require("../config/etapes_mp");
const db = require("../db");

exports.name = "newuser";

exports.commands = [
	"/resetGroups : reset les groupes pour les années/semestres et les redemandes aux utilisateurs (administrateur)"
];

exports.onMessage = async (msg) => {
	// Si l'admin décide reset les groupes
	if (msg.content == '/resetGroups BTOBBTVAKHXQFTTU' && msg.author.id == adminUser) {
		await resetGroups(msg.client, true);
		return true;
	}
	if (msg.content == '/resetGroups addRole' && msg.author.id == adminUser) {
		await resetGroupsAddRole(msg.client, true);
		return true;
	}
	if (msg.content == '/resetGroups removeRole' && msg.author.id == adminUser) {
		await resetGroupsRemoveRole(msg.client, true);
		return true;
	}
	if (msg.content == '/resetGroups' && msg.author.id == adminUser) {
		await resetGroups(msg.client, false);
		return true;
	}

	// Si le message provient d'un nouvel utilisateur
	if (msg.channel.type == "dm" && db.isNewUser(msg.author.id)) {
		await onNewUserMessage(msg);
		return true;
	}

	return false;
}

exports.onNewUser = async (newUser) => {
	db.setNewUserDetails(newUser.id, {
		step: 'eleve_annee',
		semestre: null,
		groupe: null,
		name: null
	});

	await newUser.send("Salut ! Bienvenue sur le serveur de l'IUT ! ");

	await sendMessageToNewUser(newUser);

	return true;
}

async function sendMessageToNewUser(newUser) {
	// on voit a quel étape de l'inscription est l'utilisateur
	const etape = etapes[db.getNewUserDetails(newUser.id).step];

	// on lui envoie le message correspondant à l'étape
	await newUser.send(`${etape.message}` + (etape.expected ? `\nRéponses attendues : \`${etape.expected.join("/")}\`` : ""));
}

async function onNewUserMessage(msg) {
	let newUser = db.getNewUserDetails(msg.author.id)

	// on récupère l'étape de l'utilisateur
	let s = newUser.step;

	// on vire les accents et les blancs
	let message = msg.content
		.trim()
		.toLowerCase()
		.replace('é', 'e')
		.replace('è', 'e')
		.replace('ê', 'e')
		.replace('à', 'a');

	// si ce qu'a envoyé l'utilisateur n'est pas conforme à la réponse attendue
	if (etapes[s].expected && !etapes[s].expected.includes(message)) {
		await msg.author.send("Désolé, je ne comprends pas ça...");
		return;
	}
	if (etapes[s].min_char && etapes[s].max_char && (message.length > etapes[s].max_char || message.length < etapes[s].min_char)) {
		await msg.author.send(`Désolé, ta réponse doit faire entre ${etapes[s].min_char} et ${etapes[s].max_char} caractères...`);
		return;
	}

	// on stocke les bonnes variables dans le profil de l'élève
	if (etapes[s].variable == 'semestre')
		newUser.semestre = message;
	if (etapes[s].variable == 'groupe')
		newUser.groupe = message;
	if (etapes[s].variable == 'name')
		newUser.name = message;

	// on vérifie s'il y a une ou plusieurs suites à l'inscription
	if (typeof etapes[s].next == 'object')
		newUser.step = etapes[s].next[message];
	else
		newUser.step = etapes[s].next;

	// on lui envoie le message pour la suite/fin
	await sendMessageToNewUser(msg.author);

	// on supprime l'utilisateur de la liste s'il a terminé l'inscription
	if (newUser.step == 'end')
		db.deleteNewUserDetails(msg.author.id)
	// ou on sauvegarde sa progression
	else
		db.setNewUserDetails(msg.author.id, newUser)

	// si la suite == end, cela signifie qu'on a terminé l'inscription
	if (newUser.step == 'end')
		await validerInscription(msg.author, newUser, msg.client);
}


// on valide l'inscription
async function validerInscription(user, insc, client) {

	// on récupère l'id des roles correspondant a l'année et au groupe de l'utilisateur (et a la spé pour les S4)
	let role_annee_ID = null;
	let role_groupe_ID = null;
	let role_spe_ID = null;

	// si année 1
	if (insc.semestre == '1' || insc.semestre == '2') {
		role_annee_ID = roles.A1.A1;

		if (insc.groupe != "autre")
			role_groupe_ID = roles.A1[insc.groupe];
	} // si année 2
	else if (insc.semestre == '3' || insc.semestre == '4') {
		role_annee_ID = roles.A2.A2;

		// au 3e semestre
		if (insc.semestre == '3' && insc.groupe != "autre")
			role_groupe_ID = roles.A2[insc.groupe];

		// au 4e semestre, on différencie os et at
		if (insc.semestre == '4' && insc.groupe != "autre") {
			if (insc.groupe.includes("os")) {
				role_spe_ID = roles.A2.OS.OS;
				role_groupe_ID = roles.A2.OS[insc.groupe];
			}
			else if (insc.groupe.includes("at")) {
				role_spe_ID = roles.A2.AT.AT;
				role_groupe_ID = roles.A2.AT[insc.groupe];
			}
		}
	} // si LP
	else if (insc.semestre == '5' || insc.semestre == '6') {
		role_annee_ID = roles.LP.A3;

		if (insc.groupe != "autre")
			role_groupe_ID = roles.LP[insc.groupe];
	}
	else if (insc.semestre == 'ancien') // si ancien
		role_annee_ID = roles.Ancien;
	else
		role_annee_ID = roles.Autre;

	// on récupère les roles
	let role_annee = null;
	let role_groupe = null;
	let role_spe = null;

	let member = await client.guilds.cache.get(serverID).members.fetch(user.id);

	if (role_annee_ID != null)
		role_annee = await client.guilds.cache.get(serverID).roles.fetch(role_annee_ID);
	if (role_groupe_ID != null)
		role_groupe = await client.guilds.cache.get(serverID).roles.fetch(role_groupe_ID);
	if (role_spe_ID != null)
		role_spe = await client.guilds.cache.get(serverID).roles.fetch(role_spe_ID);

	// puis on lui attribue les bons roles
	if (role_annee)
		await member.roles.add(role_annee);
	if (role_groupe)
		await member.roles.add(role_groupe);
	if (role_spe_ID)
		await member.roles.add(role_spe_ID);

	// On met bien une majuscule a chaque partie du prénom + nom
	let partieNom = insc.name.split(" ");

	for (let i = 0; i < partieNom.length; i++) {
		partieNom[i] = partieNom[i][0].toUpperCase() + partieNom[i].substring(1);
	}

	insc.name = partieNom.join(" ");

	// puis on rename l'utilisateur
	await member.setNickname(insc.name);
}

async function resetGroups(client, prodMode) {
	console.log("Nouveau semestre");

	//on récupère tout les utilisateurs du serveur
	let members = await client.guilds.cache.get(serverID).members.fetch();

	//on récupère les roles pour les années et les groupes
	const rolesANePasChanger = [roles.Professeur, roles.Ancien, roles.Autre]; //les anciens, les profs et les Autres ne vont pas changer d'un semestre a l'autre
	const rolesAAvoir = [roles.ResetRole]; //les anciens, les profs et les Autres ne vont pas changer d'un semestre a l'autre

	let resetedMembers = []
	let failedMembers = []

	for (const member of members.array()) {
		try {
			// si l'utilisateur est un bot, on laisse tomber
			if (member.user.bot)
				continue;

			// on laisse tomber si l'utilisateur est un prof ou un ancien, il ne change pas d'année
			let validMember = true
			for (let [id, role] of member.roles.cache) {
				if (rolesANePasChanger.includes(id)) {
					validMember = false
				}
			}
			if (!validMember)
				continue;

			validMember = false;
			for (let [id, role] of member.roles.cache) {
				if (rolesAAvoir.includes(id)) {
					validMember = true;
				}
			}
			if (!validMember)
				continue;

			if (prodMode) {

				// sinon pour chaque role, on vérifie si on doit enlever ce role
				for (let [id, role] of member.roles.cache) {
					//ouais c'est sale mais ca marche et c'est simple a mettre en place ¯\_(ツ)_/¯
					const rolesString = JSON.stringify(roles);

					if (rolesString.includes(role.id) && role.id != roles.ResetRole)
						await member.roles.remove(role);
				}

				// on fait comme si c'etait un nouvel utilisateur
				db.setNewUserDetails(member.id, {
					step: 'eleve_annee',
					semestre: null,
					groupe: null,
					name: null
				});

				await member.send("Salut ! On est reparti pour un nouveau semestre ! :smile:");
				await sendMessageToNewUser(member);
			}

			resetedMembers.push(member);
		} catch (e) {
			client.users.cache.get(adminUser).send(new Discord.MessageEmbed()
				.setColor(0xFF0000)
				.setTitle("Erreur interne")
				.setDescription(e.stack))
		}

	}

	await client.users.cache.get(adminUser).send("Utilisateurs réinitialisés : \n\n" + resetedMembers.map(member => `- ${member.user.id} / ${member.user.username} (${member.roles.cache.array().map(role => role.name).join(" / ")})`).join("\n"), { split: true });
	await client.users.cache.get(adminUser).send("Utilisateurs non réinitialisés : \n\n" + failedMembers.map(member => `- ${member.user.id} / ${member.user.username} (${member.roles.cache.array().map(role => role.name).join(" / ")})`).join("\n"), { split: true });
}

async function resetGroupsAddRole(client) {
	let members = await client.guilds.cache.get(serverID).members.fetch();

	const rolesANePasChanger = [roles.Professeur, roles.Ancien, roles.Autre]; //les anciens, les profs et les Autres ne vont pas changer d'un semestre a l'autre
	const roleArajouter = (await client.guilds.cache.get(serverID).roles.fetch()).cache.get(roles.ResetRole)

	for (const member of members.array()) {
		if (member.user.bot)
			continue;

		let validMember = true
		for (let [id, role] of member.roles.cache) {
			if (rolesANePasChanger.includes(id)) {
				validMember = false
			}
		}
		if (!validMember)
			continue;

		await member.roles.add(roleArajouter);
	}

	await client.users.cache.get(adminUser).send("done");
}

async function resetGroupsRemoveRole(client) {
	let members = await client.guilds.cache.get(serverID).members.fetch();
	for (const member of members.array()) {
		if (member.user.bot)
			continue;

		for (let [id, role] of member.roles.cache) {
			if (id == roles.ResetRole) {
				await member.roles.remove(role);
			}
		}
	}

	await client.users.cache.get(adminUser).send("done");
}
