
const Discord = require("discord.js")
const db = require("../db")

exports.name = "groups"

exports.commands = [
    "/group list : affiche la liste des rôles disponibles",
    "/group add {rôle} : permet de s'ajouter un rôle",
    "/group remove {rôle} : permet de supprimer un rôle qu'on possède",
    "/group db_add {rôle} : ajoute un rôle à la base de données du bot (administrateur)",
    "/group db_remove {rôle} : supprime un rôle de la base de données du bot (administrateur)",
]

exports.onMessage = async msg => {

    let params = msg.content.trim().split(' ')

    if (params[0] != '/group')
        return false

    // Ajout d'un role dand la BDD
    if (params.length >= 3 && params[1] == 'db_add') {
        if (!db.isAdmin(msg.author.id)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Vous n'êtes pas autorisé à utiliser cette commande !`))
            return true
        }

        const roleName = params.splice(2).join(" ")

        if (db.hasRole(roleName)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Le rôle "${roleName}" est déjà existant !`))
            return true
        }

        db.addRole(roleName)

        msg.channel.send(new Discord.MessageEmbed()
            .setColor(0x00FF00)
            .setDescription(`Le rôle "${roleName}" a été ajouté à la liste !`))

        return true
    }

    // Suppression d'un rôle dans la BDD
    if (params.length >= 3 && params[1] == 'db_remove') {
        if (!db.isAdmin(msg.author.id)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Vous n'êtes pas autorisé à utiliser cette commande !`))
            return true
        }

        const roleName = params.splice(2).join(" ")

        if (!db.hasRole(roleName)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Le rôle "${roleName}" n'est pas dans la liste, et ne peut donc pas être supprimé !`))
            return true
        }

        db.removeRole(roleName)

        msg.channel.send(new Discord.MessageEmbed()
            .setColor(0x00FF00)
            .setDescription(`Le rôle "${roleName}" a été supprimé de la liste !`))

        return true
    }

    // Liste des groupes
    if (params.length == 2 && params[1] == 'list') {
        const embed = new Discord.MessageEmbed()
            .setColor(0xFFFFFF)
            .setTitle('Liste des groupes')
            .setDescription(db.getRoles().sort().join('\n'))

        msg.channel.send(embed)

        return true
    }

    // Ajout d'un rôle
    if (params.length >= 3 && params[1] == 'add') {
        const roleName = params.splice(2).join(" ")

        if (!db.hasRole(roleName)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Ce rôle "${roleName}" n'existe pas !`))
            return true
        }

        let roles = await msg.guild.roles.fetch()
        roles = roles.cache.filter(role => role.name == roleName)
        await msg.member.roles.add(roles)

        msg.channel.send(new Discord.MessageEmbed()
            .setColor(0x00FF00)
            .setDescription(`Le rôle "${roleName}" vous a été ajouté !`))

        return true
    }

    // Suppression d'un rôle
    if (params.length >= 3 && params[1] == 'remove') {
        const roleName = params.splice(2).join(" ")

        if (!db.hasRole(roleName)) {
            msg.channel.send(new Discord.MessageEmbed()
                .setColor(0xFF0000)
                .setDescription(`Ce rôle "${roleName}" n'existe pas !`))
            return true
        }

        let roles = await msg.guild.roles.fetch()
        roles = roles.cache.filter(role => role.name == roleName)
        await msg.member.roles.remove(roles)

        msg.channel.send(new Discord.MessageEmbed()
            .setColor(0x00FF00)
            .setDescription(`Le rôle "${roleName}" vous a été supprimé !`))

        return true
    }

}
