# Bot Discord

## Installation

```bash
git clone git@gitlab.com:jonathanjdev/discordbot.git
cd discordbot
npm install
```

## Configuration

La configuration se trouve dans le ficher `config/config.json` qu'il faut créer avec le template suivant :

```json
{
    "token": "botToken",
    "adminUser": "adminUserId",
    "modules": {
        "groups": {
            "channels": [
                "id",
                "id",
                "..."
            ]
        },
        "help": {
            "channels": [
                "id",
                "id",
                "..."
            ]
        }
    }
}
```

## Lancement

```bash
npm start
```
