
const config = require("./config/config")
const Discord = require("discord.js")

const modules = [
    require("./modules/groups"),
    require("./modules/help"),
    require("./modules/nickname"),
    require("./modules/newuser")
]

const client = new Discord.Client()

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`)
});

client.on('message', async msg => {
    try {
        if (msg.author.bot)
            return;

        for (const module of modules) {
            // Vérification de l'écoute du bot sur ce channel
            if (config.modules[module.name] && config.modules[module.name].channels && !config.modules[module.name].channels.includes(msg.channel.id))
                continue

            if (await module.onMessage(msg, client, modules)) return
        }
    } catch (e) {
        msg.channel.send(new Discord.MessageEmbed()
            .setColor(0xFF0000)
            .setTitle("Erreur interne")
            .setDescription(e.stack))
    }
});


client.on('guildMemberAdd', async member => {

    try {
        for (const module of modules) {
            if (!module.onNewUser)
                continue

            if (await module.onNewUser(member)) return
        }
    } catch (e) {
        client.users.cache.get(config.adminUser).send(new Discord.MessageEmbed()
            .setColor(0xFF0000)
            .setTitle("Erreur interne")
            .setDescription(e.stack))
    }
});

client.login(config.token)
