
const config = require("./config/config.json")
const JsonDB = require("node-json-db").JsonDB
const Config = require("node-json-db/dist/lib/JsonDBConfig").Config

const db = new JsonDB(new Config(__dirname + "/db", true, true, '/'))

//////////////////////
// Admin
//////////////////////

const isAdmin = userId => {
    return userId == config.adminUser
}

//////////////////////
// Roles
//////////////////////

const getRoles = () => {
    try {
        return db.getData("/roles")
    } catch (error) {
        return []
    };
}

const hasRole = roleName => {
    return getRoles().find(name => name == roleName) != undefined
}

const addRole = roleName => {
    db.push("/roles[]", roleName, false)
}

//////////////////////
// NewUser
//////////////////////

const isNewUser = userId => {
    try {
        return !!db.getData(`/new_users/${userId}`)
    } catch (error) {
        return false
    };
}

const getNewUserDetails = userId => {
    return db.getData(`/new_users/${userId}`)
}

const setNewUserDetails = (userId, details) => {
    db.push(`/new_users/${userId}`, details)
}

const deleteNewUserDetails = userId => {
    db.delete(`/new_users/${userId}`);
}

//////////////////////
// Export
//////////////////////

module.exports = {
    isAdmin,

    getRoles,
    hasRole,
    addRole,

    isNewUser,
    getNewUserDetails,
    setNewUserDetails,
    deleteNewUserDetails,
}
